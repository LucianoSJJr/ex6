/***************************************************************************
 *   ex6_b.c                                  Version 20190918.221551      *
 *                                                                         *
 *   Programa que simula metodo merge sort                                 *
 *   Copyright (C) 2019         by Luciano Jose Da Silva Junior            *
 *   Copyright (C) 2019         by Raquel Gomes Da Silva                   *
 *   Copyright (C) 2019         by Maria Eduarda De Acioli                 *
 *   Copyright (C) 2019         by Juliane Rocha Macedo Coutinho           *
 ***************************************************************************
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; version 2 of the License.               *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************
 *   To contact the authors, please write to:                              *
 *   Luciano Jose Da Silva Junior                                          *
 *   Email: lucianoxdjunior3@gmail.com                                     *
 *   Webpage: http://beco.poli.br/lucianoxdjunior3@gmail.com               *
 *   Phone: (81) 98703-8887                                                *
 ***************************************************************************/

/*
 * Instrucoes para compilar:
 * $gcc ex6_b.c -o ex6_b.x -Wall -Wextra -g -O0 -DDEBUG=1
 * opcoes extras: -ansi -pedantic-errors
 */

/* ---------------------------------------------------------------------- */
/* includes */

#include <stdio.h> /* Standard I/O functions */
#include <stdlib.h> /* Miscellaneous functions (rand, malloc, srand)*/
#include <time.h>  /* Time and date functions */

/* ---------------------------------------------------------------------- */
/* definitions */

#ifndef VERSION /* gcc -DVERSION="0.1.160612.142306" */
#define VERSION "20190918.221551" /* Version Number (string) */
#endif

/* Debug */
#ifndef DEBUG /* gcc -DDEBUG=1 */
#define DEBUG 0 /* Activate/deactivate debug mode */
#endif

#if DEBUG==0
#define NDEBUG
#endif

/* Debug message if DEBUG on */
#define IFDEBUG(M) if(DEBUG) fprintf(stderr, "[DEBUG file:%s line:%d]: " M "\n", __FILE__, __LINE__); else {;}

/* limits */
#define TAM 500

/* ---------------------------------------------------------------------- */
/* prototypes */

void help(void); /* print some help */
void copyr(void); /* print version and copyright information */

/*funcao recursiva que divide o vetor em 2 metades*/
void divide(int vetor[], int size); 

/*ordena os pequenos grupos produzidos pela funcao divide e junta (merge) eles de novo*/
void merge(int v[], int s[], int tamV, int tamS, int vetor[]); 

/* ---------------------------------------------------------------------- */
int main(void)
{
    /* variaveis */ 
    int vet[TAM];
    int max = 1000;
    int i = 0;
    void troca(int *x, int *y);
    void printa(int a[],int size);  
    void selec(int a[],int n);
    int a[500];
    int w=0;
    int n=sizeof(a)/sizeof(a[0]);

    srand(time(NULL));

    help();
    copyr();

    for(i=0;i<TAM;i++)
        vet[i] = rand()%max;

    divide(vet,TAM);

    printf("MERGE SORT: \n");
    for(i=0;i<TAM;i++)
        printf("%d ",vet[i]);
    printf("\n");
    srand(time(NULL));
    for(w=0;w<500;w++)
    {
        a[w]=rand()%1000;
    }
    selec(a,n);
    printf("SELECT SORT: \n");
    printa(a,n);

    return EXIT_SUCCESS;
}
/* ---------------------------------------------------------------------- */
void divide(int vetor[], int size)
{
    int aux1 = 1;
    int v[size],s[size];
    int i = 0,j = 0;

    if(size==1)
        return;

    aux1 = size/2;

    for(i=0; i<aux1; i++)
        v[i] = vetor[i];

    for(i=aux1; i<size; i++)
    {
        s[j] = vetor[i];
        j++;
    }

    divide(v,aux1);
    divide(s,size-aux1);
    merge(v,s,aux1,size-aux1,vetor);
}
/* ---------------------------------------------------------------------- */
void merge(int v[], int s[], int tamV, int tamS, int vetor[])
{
    int i = 0, k = 0, j = 0;

    while(i<tamV && j<tamS)
    {
        if(v[i]<=s[j])
        {
            vetor[k] = v[i];
            i++;
        }
        else
        {
            vetor[k] = s[j];
            j++;
        }
        k++;
    }

    while(j<tamS)
    {
        vetor[k] = s[j];
        j++;
        k++;
    }
    while(i<tamV)
    {
        vetor[k] = v[i];
        i++;
        k++;
    }

    return;
}

void troca(int *x, int *y)
{
    int t=*x;
    *x=*y;
    *y=t;
}

void selec(int a[],int n)
{
    int i, j, min;

    for(i=0; i<n-1; i++)
    {
        min=i;
        for(j=1+i; j<n; j++)
            if(a[j]<a[min])
                min=j;
        troca(&a[min], &a[i]);
    }

}
void printa(int a[], int n)
{
    int e;
    for(e=0;e<n;e++)
        printf("%d ", a[e]);
    printf("\n");
}

/* ---------------------------------------------------------------------- */
/* Prints help information 
 *  usually called by opt -h or --help
 */
void help(void)
{
    IFDEBUG("help()");
    printf("%s - %s\n", "ex6_b", "Programa que simula metodo comb sort");
    printf("\nUsage: %s\n\n", "ex6_b");
    printf("Esse programa utiliza o metodo de ordenacao merge\n");
    printf("\nExit status:\n\t0 if ok.\n\t1 some error occurred.\n");
    printf("\nTodo:\n\tLong options not implemented yet.\n");
    printf("\nAuthor:\n\tWritten by %s <%s>\n\n", "Juliane Rocha Macedo Coutinho", "juliane.coutinho734@gmail.com");
    return;
}

/* ---------------------------------------------------------------------- */
/* Prints version and copyright information 
 *  usually called by opt -V
 */
void copyr(void)
{
    IFDEBUG("copyr()");
    printf("%s - Version %s\n", "ex6_b", VERSION);
    printf("\nCopyright (C) %d %s <%s>, GNU GPL version 2 <http://gnu.org/licenses/gpl.html>. This  is  free  software: you are free to change and redistribute it. There is NO WARRANTY, to the extent permitted by law. USE IT AS IT IS. The author takes no responsability to any damage this software may inflige in your data.\n\n", 2019, "Juliane Rocha Macedo Coutinho", "juliane.coutinho734@gmail.com");
    return;
}

/* ---------------------------------------------------------------------- */
/* vi: set ai et ts=4 sw=4 tw=0 wm=0 fo=croql : C config for Vim modeline */
/* Template by Dr. Beco <rcb at beco dot cc> Version 20160612.142044      */

